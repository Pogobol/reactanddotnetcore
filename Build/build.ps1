﻿#######How to use this file##########
#1. Update your .net framework version in the parameter -framework. Use 4.6 for 4.6.1 or 4.6.2 (e.g. --framework 4.6 ` )
#2. Run this scripts with PowerShell to test the results
#####################################

param()

cls

#setup variables
$NugetFeeds = "https://api.nuget.org/v3/index.json;http://localhost:8081/guestAuth/app/nuget/v1/FeedService.svc/" #nuget feeds

$packagesList = Get-ChildItem -Filter packages.config -Recurse       #get packages.config from all projects
$solutionFile = Get-ChildItem -Filter *.sln | Select-Object -First 1 #get solution file
$solutionDirectory = (Get-Item $solutionFile).DirectoryName          
$packagesDirectory = "$solutionDirectory\packages"
$nugetExe = Get-ChildItem -Filter Build\Nuget\Nuget.exe | Select-Object -First 1 #Nuget command line Executable

#download nuget packages
 foreach ($packageFile in $packagesList) {
   &$nugetExe.FullName install $packageFile.FullName -source $NugetFeeds  -NonInteractive -RequireConsent -solutionDir $solutionDirectory
 }


# '[p]sake' is the same as 'psake' but $Error is not polluted
remove-module [p]sake

# find psake's path
$psakeModule = (Get-ChildItem (".\Packages\psake*\tools\psake.psm1")).FullName | Sort-Object $_ | select -last 1

$psakeScript = (Get-ChildItem (".\Packages\PsBuildScripts*\tools\buildScripts.ps1")).FullName | Sort-Object $_ | select -last 1
 
 
Import-Module $psakeModule

# you can write statements in multiple lines using `
Invoke-psake -buildFile $psakeScript `
			 -taskList Clean `
			 -framework 4.6 `
		     -properties @{ 
				 "buildConfiguration" = "Release"
				 "buildPlatform" = "Any CPU"} `
			 -parameters @{ 
				 "solutionFile"= Resolve-Path(".\$solutionFile")}

Write-Host "Build exit code:" $LastExitCode

# Propagating the exit code so that builds actually fail when there is a problem
exit $LastExitCode