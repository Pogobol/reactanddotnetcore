﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Imobium.Web.Api.Identity.Infrastructure
{
    public class ImobiumIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public ImobiumIdentityDbContext()
            : base("ImobiumConnectionString", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static ImobiumIdentityDbContext Create()
        {
            return new ImobiumIdentityDbContext();
        }

    }
}