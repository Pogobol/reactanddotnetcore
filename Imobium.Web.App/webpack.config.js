const path = require("path");
const webpack = require("webpack");

module.exports = {
  context: path.resolve(__dirname, "./src"),
  entry: [
    path.resolve(__dirname, "./src/index.js"),
  ],
  output: {
    path: path.resolve(__dirname, "./dist/assets/js"),
    filename: "bundle.js",
    publicPath: "/"
  },
  watch: false,
  devServer: {
    inline: true,
    contentBase: "./dist",
    port: 3000
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/(node_modules)/],
        use: [
          {
            loader: "babel-loader",
            options: { presets: ["es2015", "stage-1", "react"] }
          }
        ]
      }
    ]
  }
};
