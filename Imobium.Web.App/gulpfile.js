﻿'use restrict';
var gulp = require('gulp');
var less = require("gulp-less");
var minifyCss = require("gulp-clean-css");
var rename = require("gulp-rename");
var path = require("path");

var paths = {
    dist: __dirname + '/dist/',
    source: {
        less: __dirname + '/src/assets/less/'
    }
}

gulp.task('build-less-dev', function () {
    console.log('path', paths);
    console.log('paths.source.less', paths.source.less);
    console.log('_main_full',paths.source.less + '_main_full/');
    return gulp
        .src(paths.source.less + '_main_full/*.less')
        .pipe(less())
        .pipe(gulp.dest(paths.dist + "assets/css"));
});

gulp.task('build-less-prod', function () {
    return gulp
        .src(paths.source.less + '_main_full/*.less')
        .pipe(less())
        .pipe(minifyCss({ 
            keepSpecialComments: 0 // remove all comments
        }))
        .pipe(rename({ 
            suffix: ".min" // add *.min suffix
        }))
        .pipe(gulp.dest(paths.dist + "assets/css"));
});

gulp.task('watch-less',['build-less-dev'], function () {
    //gulp.watch(paths.source.less + '**/*.less', ['build-less-dev']); // listen for changes in all LESS files and automatically re-compile
});

gulp.task('default', ['build-less-dev','build-less-prod'], function () {
    // place code for your default task here
});